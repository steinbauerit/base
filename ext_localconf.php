<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSConfig/Page.ts">');

$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = serialize([
	'loginLogo' => 'EXT:base/Resources/Public/Images/logo.png',
	'loginHighlightColor' => '#ef9939',
	'loginBackgroundImage' => 'EXT:base/Resources/Public/Images/background.png',
	'backendLogo' => 'EXT:base/ext_icon.png'
]);

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['lang']['availableLanguages'] = ['de'];

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['base'] = 'EXT:base/Configuration/RTE/Default.yaml';

if (TYPO3_MODE === 'BE') {
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \SteinbauerIT\Base\Command\BaseCommandController::class;
}
