tx_gridelements.setup.column3 {
  title = 3 Spalter
  #icon = EXT:example_extension/Resources/Public/Images/BackendLayouts/default.gif
  config {
    colCount = 3
    rowCount = 1
    rows {
      1 {
        columns {
          1 {
            name = Links
            colPos = 0
            allowed = *
          }
          2 {
            name = Mitte
            colPos = 1
            allowed = *
          }
          3 {
            name = Rechts
            colPos = 2
            allowed = *
          }
        }
      }
    }
  }
}