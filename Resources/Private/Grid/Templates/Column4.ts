tx_gridelements.setup.column4 {
  title = 4 Spalter
  #icon = EXT:example_extension/Resources/Public/Images/BackendLayouts/default.gif
  config {
    colCount = 4
    rowCount = 1
    rows {
      1 {
        columns {
          1 {
            name = Links
            colPos = 0
            allowed = *
          }
          2 {
            name = Mitte link
            colPos = 1
            allowed = *
          }
          3 {
            name = Mitte rechts
            colPos = 2
            allowed = *
          }
          4 {
            name = Rechts
            colPos = 3
            allowed = *
          }
        }
      }
    }
  }
}