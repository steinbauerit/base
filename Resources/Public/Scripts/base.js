// Cookie consent
window.addEventListener("load", function(){
	window.cookieconsent.initialise({
		"palette": {
			"popup": {
				"background": "#252e39"
			},
			"button": {
				"background": "#14a7d0"
			}
		},
		"showLink": false,
		"content": {
			"message": "Cookies erleichtern die Bereitstellung unserer Dienste. Mit der Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies verwenden.",
			"dismiss": "Verstanden"
		}
	})
});

// Lighcase lightbox
jQuery(document).ready(function($) {
	$('a[data-rel^=lightcase]').lightcase({
		maxWidth: 2000,
		maxHeight: 2000,
		swipe: true,
		labels: {
			'sequenceInfo.of': ' / '
		}
	});
});