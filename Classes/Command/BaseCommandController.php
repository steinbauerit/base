<?php
namespace SteinbauerIT\Base\Command;

use \TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

class BaseCommandController extends CommandController
{
    /**
     * Index all file of default storage (fileadmin, id = 1)
     */
    public function indexFilesCommand()
    {
    	$storage = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance()->getStorageObject(1);
        $storage->setEvaluatePermissions(false);
        $indexer = $this->getIndexer($storage);
        $indexer->processChangesInStorages();
        $storage->setEvaluatePermissions(true);
        $this->outputLine('Files indexed.');
        $this->outputLine();
    }

    /**
     * Gets the indexer
     *
     * @param \TYPO3\CMS\Core\Resource\ResourceStorage $storage
     * @return \TYPO3\CMS\Core\Resource\Index\Indexer
     */
    protected function getIndexer(\TYPO3\CMS\Core\Resource\ResourceStorage $storage)
    {
        return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\Index\Indexer::class, $storage);
    }
}