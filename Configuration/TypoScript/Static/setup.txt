# Include all TS files
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:base/Configuration/TypoScript/" extensions="ts">

tt_content.gridelements_pi1.20.10.setup {
	<INCLUDE_TYPOSCRIPT: source="DIR:EXT:base/Resources/Private/Grid/Templates" extensions="txt">
}
