plugin.tx_vhs.settings.asset {
    jquery {
        path = https://code.jquery.com/jquery-3.3.1.min.js
        external = 1
    }
    popperJs {
        path = https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js
        external = 1
        dependencies = jquery
    }
    bootstrapJs {
        path = https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js
        external = 1
        dependencies = popperJs
    }
    lightcaseJs {
        path = https://cdnjs.cloudflare.com/ajax/libs/lightcase/2.4.2/js/lightcase.min.js
        external = 1
        dependencies = jquery
    }
    cookieconsentJs {
        path = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js
        external = 1
    }
    baseJs {
        path = EXT:base/Resources/Public/Scripts/base.js
        dependencies = jquery,lightcaseJs,cookieconsentJs
    }

    bootstrapCss {
        path = https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css
        external = 1
    }
    lightcaseCss {
        path = https://cdnjs.cloudflare.com/ajax/libs/lightcase/2.4.2/css/lightcase.min.css
        external = 1
    }
    cookieconsentCss {
        path = https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css
        external = 1
    }
    baseCss {
        path = EXT:base/Resources/Public/Css/base.css
        dependencies = lightcaseCss,cookieconsentCss
    }
}
