page = PAGE
page {
   1 = LOAD_REGISTER
   1 {
      pageLayout.cObject = TEXT
      pageLayout.cObject {
         data = levelfield:-1, backend_layout_next_level, slide
         override.field = backend_layout
         split {
            token = pagets__
            1.current = 1
            1.wrap = |
         }
      }
   }
   10 = FLUIDTEMPLATE
   10 {
      templateName = TEXT
      templateName.data = register:pageLayout
      format = html
      templateRootPaths {
         10 = EXT:base/Resources/Private/Page/Templates
      }
      partialRootPaths {
         10 = EXT:base/Resources/Private/Page/Partials
      }
      layoutRootPaths {
         10 = EXT:base/Resources/Private/Page/Layouts
      }
      variables {
         layout = TEXT
         layout.data = register:pageLayout
      }
   }
   bodyTagCObject = TEXT
   bodyTagCObject {
      wrap = <body class="|">
      data = register:pageLayout
   }
   meta.viewport  = width=device-width, initial-scale=1.0
}

lib.content0 < styles.content.get
lib.content1 < styles.content.getLeft
lib.content2 < styles.content.getRight
lib.content3 < styles.content.getBorder
lib.content4 < styles.content.get
lib.content4.select.where = colPos=4
lib.content5 < styles.content.get
lib.content5.select.where = colPos=5

lib.contentElement.templateRootPaths.10 = EXT:base/Resources/Private/CoreContent/Templates/
lib.contentElement.partialRootPaths.10 = EXT:base/Resources/Private/CoreContent/Partials/
lib.contentElement.layoutRootPaths.10 = EXT:base/Resources/Private/CoreContent/Layouts/

tt_content.image.dataProcessing.20.maxGalleryWidthInText = 2000
tt_content.image.dataProcessing.20.maxGalleryWidth = 2000
tt_content.textmedia.dataProcessing.20.maxGalleryWidthInText = 2000
tt_content.textmedia.dataProcessing.20.maxGalleryWidth = 2000

lib.gridelements.baseSetup < lib.gridelements.defaultGridSetup
lib.gridelements.baseSetup.cObject = FLUIDTEMPLATE
lib.gridelements.baseSetup.cObject {
   templateName = Default
   format = html
   templateRootPaths {
      10 = EXT:base/Resources/Private/Grid/Templates
   }
   partialRootPaths {
      10 = EXT:base/Resources/Private/Grid/Partials
   }
   layoutRootPaths {
      10 = EXT:base/Resources/Private/Grid/Layouts
   }
}

config.tx_realurl_enable = 1
