# include all the backend layouts
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:base/Resources/Private/Page/Templates" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:base/Resources/Private/Grid/Templates" extensions="ts">

# Remove default backend layout from selectors
TCEFORM.pages.backend_layout_next_level.removeItems = -1
TCEFORM.pages.backend_layout.removeItems = -1

# Disable unused fields
TCEFORM.tt_content {
	date.disabled = 1
	layout.disabled = 1
	frame_class.disabled = 1
	sectionIndex.disabled = 1
	linkToTop.disabled = 1
	imageborder.disabled = 1
	cols.disabled = 1
	table_class.disabled = 1
	table_header_position.disabled = 1
	table_tfoot.disabled = 1
	target.disabled = 1
	uploads_description.disabled = 1
	header_layout {
		label = Überschrift-Typ
		removeItems = 0, 100
		altLabels {
			1 = Überschrift 1
			2 = Überschrift 2
			3 = Überschrift 3
			4 = Überschrift 4
			5 = Überschrift 5
		}
		addItems.6 = Überschrift 6
	}
	space_before_class {
		label = Abstand davor
		keepItems = 
		addItems {
			default = Standard
			extra-small = Keiner
			small = Klein
			medium = Mittel
			large = Groß
			extra-large = Sehr groß
		}
	}
	space_after_class {
		label = Abstand danach
		keepItems = 
		addItems {
			default = Standard
			extra-small = Keiner
			small = Klein
			medium = Mittel
			large = Groß
			extra-large = Sehr groß
		}
	}
	table_caption.label = Tabellen-Titel
	imageorient.removeItems = 8, 9, 10, 17, 18, 25, 26
	CType {
		removeItems = textpic, bullets
	}
}

mod.wizards.newContentElement.wizardItems.common.elements.image.title = Bilder
mod.wizards.newContentElement.wizardItems.common.elements.image.description = Eine beliebige Anzahl von Bildern mit Beschriftung, optional in einer Gallerie.
mod.wizards.newContentElement.wizardItems.common.elements.textmedia.title = Medien
mod.wizards.newContentElement.wizardItems.common.elements.textmedia.description = Eine beliebige Anzahl von Medien mit Beschriftung.

RTE.default.preset = base

tx_gridelements.overruleRecords = 1
