<?php
	$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][4] = [
        'Bilder',
        'image',
        'content-image'
    ];
    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][5] = [
        'Medien',
        'textmedia',
        'content-textmedia'
    ];

    $GLOBALS['TCA']['tt_content']['types']['textmedia']['columnsOverrides'] = [
        'image_zoom' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'bodytext' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'tx_base_gallery_type' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ]
    ];

    $GLOBALS['TCA']['tt_content']['types']['gridelements_pi1']['columnsOverrides'] = [
        'header' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header_layout' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header_position' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header_link' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'media' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ]
    ];

    $galleryType = array(
        'tx_base_gallery_type' => array (
            'exclude' => 0,
            'label' => 'Klick-Vergrößern Galerie-Typ',
            'config' => array (
                'type' => 'select',
                'items' => array (
                        array('Normal', '0'),
                        array('Seitenweite Galerie', '1'),
                        array('Keine Gallerie', '2'),
                ),
                'size' => 1,
                'maxitems' => 1,
        	)
        )
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $galleryType
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'imagelinks',
        'tx_base_gallery_type',
        'after:image_zoom'
	);
